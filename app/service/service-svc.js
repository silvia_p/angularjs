modulo.service('provaService', ['$http', '$log', 'DialogService', function ($http, $log, DialogService) {

    //ecma5
    service.orderCall = function (valori, order, isToReverse) {
        return $http.put(urlCard + order + '/' + isToReverse + '/orderDocumentsDettCardByParam', valori)
            .success(function (dettaglioModificato, status, headers, config) {
                //tutto ok
            })
            .error(function (data, status, headers, config) {
                // lancio modalino errore
                DialogService.alertErroreGenerico(data);
            });
            
    }
    //ecma6
    service.initPage = function (id) {
        let url = baseUrl + '/id/' + id;
        return $http.get(url).error((data) => DialogService.alertErroreGenerico(data));
    };


    //slide 52
    $http.get(/*...*/).
        then(function seqFunc1(response) {/*...*/ }).
        then(function seqFunc2(response) {/*...*/ })

    $http.get()
    seqFunc1()
    seqFunc2()

    $http(/*...*/).
        success(function parFunc1(data) {/*...*/ }).
        success(function parFunc2(data) {/*...*/ })

    $http.get()
    parFunc1(), parFunc2() in parallel

}]);