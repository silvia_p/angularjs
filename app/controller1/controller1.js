var modulo = angular.module('myApp');
modulo.controller('controller1Ctrl', ['$scope', '$rootScope', '$filter', '$routeParams', '$location', function ($scope, $rootScope, $filter, $routeParams, $location) {

  var vm = this;
  vm.nomePersona = "Ctrl1";
  vm.rootScope = $rootScope;
  vm.nomeInput = "Default";
  vm.disableNomeInput = true;
  vm.showNome = false;
  vm.lista = ["Silvia", "Luca", "Carlo", "Maria", "Paolo"];
  vm.lista = $filter('orderBy')(vm.lista);
  vm.limitToNumber = 2345432342;
  vm.listaObj = [];

  //function
  vm.showNomeInserito = showNomeInserito;
  vm.enableInput = enableInput;
  vm.checkNomeInput = checkNomeInput;
  vm.initList = initList;
  vm.goToView = goToView;

  vm.initList();

  //slide 39: recupero parametro
  if ($routeParams.id) {
    vm.nomePersona = $routeParams.id;
  }

  //Slide 33: ng-click
  function enableInput() {
    vm.disableNomeInput = false;
  }

  function checkNomeInput() {
    if (vm.nomeInput == "disable") {
      vm.disableNomeInput = true;
    }
  }

  //Slide 33: ng-change
  function showNomeInserito(checkVar) {
    let show = true;
    if (!checkVar) {
      show = false;
    }
    vm.showNome = show;
  }

  function initList() {
    let obj1 = {
      name: "Silvia",
      surname: "Pietropoli"
    }
    let obj2 = {
      name: "Marco",
      surname: "Bianchi"
    }
    let obj3 = {
      name: "Anna",
      surname: "Rossi"
    }
    vm.listaObj.push(obj1);
    vm.listaObj.push(obj2);
    vm.listaObj.push(obj3);
  }

  function goToView(param) {
    let limit = param ? param : 0;
    $location.url($location.path() + "viewTable" + limit);
    $location.path("/viewTable/" + limit);
  }


  //slide 25: esempi di non utilizzo
  //esempio di come non usarlo --> ci saranno i filtri
  //vm.nomePersona = "Silvia".toUpperCase();
  //esempio di come non usarlo --> si usano le direttive
  //   if(vm.nomePersona === "SILVIA") {
  //     document.getElementById("hello1").style.color = "red";
  //   }


}]);

modulo.controller('child1Ctrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
  var child1Ctrl = this;
  //esempio base - inizializza scope
  child1Ctrl.nomeChild = "Child1";
  child1Ctrl.nomeParent = $scope.ctrl1.nomePersona;
}]);