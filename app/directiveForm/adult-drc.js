modulo.directive('adult', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            control.$validators.adult = function (modelValue, viewValue) {

                if (control.$isEmpty(modelValue)) // if empty, correct value
                {
                    return true;
                }

                var age = Number(viewValue);

                if (age >= 18 && age <= 100) // correct value
                {
                    return true;
                }
                return false; // wrong value
            };
        }
    };
});