modulo.directive('country', function ($q, $timeout) {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
            // temporary data from local variable
            var countryNames = ['India', 'USA', 'Canada'];
            control.$asyncValidators.country = function (modelValue, viewValue) {
                if (control.$isEmpty(modelValue)) {
                    return $q.when();
                }

                var defer = $q.defer();

                // to imitate the behavior of service access
                // In real time, fire $http and get value then validate
                $timeout(function () {
                    if (countryNames.indexOf(modelValue) === -1) // not found
                    {
                        defer.resolve(); // wow, this country available
                    }
                    else {
                        defer.reject();
                    }
                }, 8000);

                return defer.promise;
            };
        }
    };
});