'use strict';

// Declare app level module which depends on views, and core components
var modulo = angular.module('app', [
  'ngComponentRouter',
  'ui.bootstrap'
]);
modulo.value('$routerRootComponent', 'app');
modulo.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  //$locationProvider.html5mode(true);
}]);
modulo.component('app', {
  templateUrl: 'index.html',
  $routeConfig: [
    {path: '/list', name: 'ComponentBase', component: 'componentBase'}
    // {path: '/prova/:id', name: 'Routing', component: 'routing'}
  ]
  
  
});
