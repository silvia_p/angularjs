modulo.filter('numberFilter', function () {
    return function (personList) {
        let pari = personList.filter((person) => {
            return person.age % 2 === 0;
        })
        return pari;
    }
});

modulo.filter('genderFilter', function () {
    return function (personList) {
        var newArray = personList.filter(function(item) {
            if(item.name.endsWith('o') || !item.name.endsWith('a')) {
                return item;
            }            
        });
        return newArray;
    }
})