'use strict';

// Declare app level module which depends on views, and core components
var modulo = angular.module('myApp', [
  'ngRoute',
  'ui.bootstrap'
]);

//var --> globale
//let --> locale
//const --> constante

//Inline Array Annotation
modulo.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider
    .when('/controller1/:id',
      {
        templateUrl: 'controller1/controller1.html',
        controller: 'controller1Ctrl'
      })
    .when('/controller2',
      {
        templateUrl: 'controller2/controller2.html',
        controller: 'controller2Ctrl'
      })
    .when('/viewTable/:limit',
      {
        templateUrl: 'controllerTable/controllerTable.html',
        controller: 'controllerTableCtrl'
      })
    .otherwise(
      {
        redirectTo: '/'
      });
}]);

//slide 39: routing
modulo.controller('mainCtrl', ['$location', function ($location) {
  var mainCtrl = this;

  mainCtrl.goToView = goToView;

  function goToView(route) {
    switch (route) {
      case 'v1':
        $location.url($location.path() + "/controller1/prova");
        $location.path("/controller1/" + "Cecilia");
        break;
      case 'v2':
        $location.path("/controller2");
        break;
      // case 'v3':
      //   $location.url($location.path() + "viewTable" + param);
      //   $location.path("/viewTable/" + param);
      //   break;
      default:
        break;
    }

  }

}]);

//run
// modulo.run(['$http', '$cookies', function($http, $cookies) {

//   $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;

// }]);

//Implicit Annotation deprecata
//someModule.controller('MyController', function($scope, greeter) 

//$inject Property Annotation: SUPER deprecata
// var MyController = function($scope, greeter) {
//   // ...
// }
// MyController.$inject = ['$scope', 'greeter'];
// someModule.controller('MyController', MyController);


