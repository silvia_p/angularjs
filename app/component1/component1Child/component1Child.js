modulo.component('componentChild1', {
    templateUrl: 'component1/component1Child/component1Child.html',
    controllerAs : 'ctrl1Child',
    bindings: {
        /*
           < --> one way binding
           @ --> stringhe
           = --> two way binding
           & --> passaggio funzioni
        */
       detail : '<'
       //elimina : '&'
    },
    controller: ['$scope', '$rootScope', function ($scope, $rootScope) {
        var ctrl1Child = this;
        ctrl1Child.elimina = elimina;

        function elimina() {
            $rootScope.$broadcast("$eliminaDetail", ctrl1Child.detail)
        }

    }]
});
