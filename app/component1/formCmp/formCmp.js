modulo.component('componentForm', {
    templateUrl: 'component1/formCmp/formCmp.html',
    controllerAs: 'ctrlForm',
    bindings: {

    },

    controller: ['$scope', '$rootScope', function ($scope, $rootScope) {
        var ctrlForm = this;
        ctrlForm.addElement = addElement;
        ctrlForm.obj = {};

        function addElement(obj, form) {
            if(form.$valid) {
                //console.log("in addEl")
                $rootScope.$broadcast("$addElementEvent", ctrlForm.obj);
            }
        }
    }]
});
