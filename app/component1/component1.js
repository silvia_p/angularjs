modulo.component('componentBase', {
    templateUrl: 'component1/component1.html',
    controllerAs : 'ctrl1',
    bindings: {
       // $router: '<'
    },
    $routeConfig: [
        //{path: '/list/detail', name: 'ComponentChild1', component: 'ComponentChild1'}
        // {path: '/list/add', name: 'ComponentForm', component: 'componentForm'}
    ],
    controller: ['$scope', function ($scope) {
        var ctrl1 = this;
        
        ctrl1.listaObj = [];
        ctrl1.showDetail = false;
        ctrl1.showAdd = false;
        ctrl1.prepareList = prepareList;
        ctrl1.showDetailFunct = showDetailFunct;
        ctrl1.showAddFunct = showAddFunct;
        ctrl1.elimina = elimina;
        ctrl1.addElement = addElement;
        ctrl1.refreshList = refreshList;
        ctrl1.prepareList = prepareList;

        $scope.$on('$eliminaDetail', function(event, data) {
            console.log("ciaooo");
            //elimino
            ctrl1.elimina();
        })

        $scope.$on('$addElementEvent', function(event, data) {
            ctrl1.showAddFunct();
            ctrl1.addElement(data);
        })

        function elimina() {
            ctrl1.showDetail = false;
            let idx = ctrl1.listaObj.indexOf(ctrl1.itemSelected);
            if (idx >= 0) {
                ctrl1.listaObj.splice(idx, 1);
            }
        }

        function addElement(element) {
            //console.log("prova");
            ctrl1.listaObj.push(element);
           // ctrl1.showAdd = true;
            
        }

        function refreshList() {
            //potresti voler far altro?
            ctrl1.prepareList();
        }

        ctrl1.$onInit = function() {
            ctrl1.prepareList();
        }


        function showDetailFunct(index) {
            ctrl1.itemSelected = ctrl1.listaObj[index];
            ctrl1.showDetail = ctrl1.itemSelected ? true : false;

        }

        function showAddFunct() {
            ctrl1.showAdd = ctrl1.showAdd ? false : true;
        }

        function prepareList() {
            ctrl1.listaObj = [];
            let obj1 = {
                name : "Silvia",
                surname : "Pietropoli",
                age : "28"
              }
              let obj2 = {
                name : "Marco",
                surname : "Rossi",
                age : "30"
              }
              let obj3 = {
                name : "Anna",
                surname : "Rossi",
                age : "35"
              }
              ctrl1.listaObj.push(obj1);
              ctrl1.listaObj.push(obj2);
              ctrl1.listaObj.push(obj3);
        }


        


    }]
});
